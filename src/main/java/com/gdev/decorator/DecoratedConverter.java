package com.gdev.decorator;

import com.gdev.interf.Converter;

public abstract class DecoratedConverter implements Converter {

	protected Converter decoratedConverter;

	public DecoratedConverter(Converter decoratedConverter) {
		this.decoratedConverter = decoratedConverter;
	}

	@Override
	public String convert(String text) {
		return decoratedConverter.convert(text);
	}

}
