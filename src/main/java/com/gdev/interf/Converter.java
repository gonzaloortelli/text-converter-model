package com.gdev.interf;

public interface Converter {

	String convert(String text);

}
