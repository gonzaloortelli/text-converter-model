package com.gdev.interf.impl;

import com.gdev.interf.Converter;

public class BasicConverter implements Converter {

	@Override
	public String convert(String text) {
		System.out.println("Convirtiendo..");
		return text;
	}

}
